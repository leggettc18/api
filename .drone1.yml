kind: pipeline
name: testing

workspace:
  base: /go
  path: src/code.vikunja.io/api

services:
  - name: test-mysql-unit
    image: mariadb:10
    environment:
      MYSQL_ROOT_PASSWORD: vikunjatest
      MYSQL_DATABASE: vikunjatest
  - name: test-mysql-integration
    image: mariadb:10
    environment:
      MYSQL_ROOT_PASSWORD: vikunjatest
      MYSQL_DATABASE: vikunjatest
  - name: test-mysql-migration
    image: mariadb:10
    environment:
      MYSQL_ROOT_PASSWORD: vikunjatest
      MYSQL_DATABASE: vikunjatest
  - name: test-postgres-unit
    image: postgres:12
    environment:
      POSTGRES_PASSWORD: vikunjatest
      POSTGRES_DB: vikunjatest
  - name: test-postgres-integration
    image: postgres:12
    environment:
      POSTGRES_PASSWORD: vikunjatest
      POSTGRES_DB: vikunjatest
  - name: test-postgres-migration
    image: postgres:12
    environment:
      POSTGRES_PASSWORD: vikunjatest
      POSTGRES_DB: vikunjatest

trigger:
  branch:
    include:
      - master
  event:
    include:
      - push
      - pull_request

steps:
  - name: fetch-tags
    image: docker:git
    commands:
      - git fetch --tags

  - name: build
    image: vikunja/golang-build:latest
    pull: true
    environment:
      GOFLAGS: '-mod=vendor'
    commands:
      - make build
    when:
      event: [ push, tag, pull_request ]

  - name: lint
    image: vikunja/golang-build:latest
    pull: true
    environment:
      GOFLAGS: '-mod=vendor'
    depends_on: [ build ]
    commands:
      - make generate
      - make lint
      - make fmt-check
      # - make got-swag # Commented out until we figured out how to get this working on drone
      - make ineffassign-check
      - make misspell-check
      - make goconst-check
      - make gocyclo-check
      - make static-check
      - wget -O - -q https://raw.githubusercontent.com/securego/gosec/master/install.sh | sh -s -- -b $GOPATH/bin v2.2.0 # Need to manually install as it does not support being installed via go modules like the rest.
      - make gosec-check
    when:
      event: [ push, tag, pull_request ]

  - name: test-migration-prepare
    image: kolaente/toolbox:latest
    pull: true
    commands:
      # Get the latest version
      - wget https://dl.vikunja.io/api/master/vikunja-master-linux-amd64-full.zip -q -O vikunja-latest.zip
      - unzip vikunja-latest.zip vikunja-master-linux-amd64

  - name: test-migration-sqlite
    image: kolaente/toolbox:latest
    pull: true
    depends_on: [ test-migration-prepare, build ]
    environment:
      VIKUNJA_DATABASE_TYPE: sqlite
      VIKUNJA_DATABASE_PATH: ./vikunja-migration-test.db
      VIKUNJA_LOG_DATABASE: stdout
      VIKUNJA_LOG_DATABASELEVEL: debug
    commands:
      - ./vikunja-master-linux-amd64 migrate
      # Run the migrations from the binary build in the step before
      - ./vikunja migrate
    when:
      event: [ push, tag, pull_request ]

  - name: test-migration-mysql
    image: kolaente/toolbox:latest
    pull: true
    depends_on: [ test-migration-prepare, build ]
    environment:
      VIKUNJA_DATABASE_TYPE: mysql
      VIKUNJA_DATABASE_HOST: test-mysql-migration
      VIKUNJA_DATABASE_USER: root
      VIKUNJA_DATABASE_PASSWORD: vikunjatest
      VIKUNJA_DATABASE_DATABASE: vikunjatest
      VIKUNJA_LOG_DATABASE: stdout
      VIKUNJA_LOG_DATABASELEVEL: debug
    commands:
      - ./vikunja-master-linux-amd64 migrate
      # Run the migrations from the binary build in the step before
      - ./vikunja migrate
    when:
      event: [ push, tag, pull_request ]

  - name: test-migration-psql
    image: kolaente/toolbox:latest
    pull: true
    depends_on: [ test-migration-prepare, build ]
    environment:
      VIKUNJA_DATABASE_TYPE: postgres
      VIKUNJA_DATABASE_HOST: test-postgres-migration
      VIKUNJA_DATABASE_USER: postgres
      VIKUNJA_DATABASE_PASSWORD: vikunjatest
      VIKUNJA_DATABASE_DATABASE: vikunjatest
      VIKUNJA_DATABASE_SSLMODE: disable
      VIKUNJA_LOG_DATABASE: stdout
      VIKUNJA_LOG_DATABASELEVEL: debug
    commands:
      - ./vikunja-master-linux-amd64 migrate
      # Run the migrations from the binary build in the step before
      - ./vikunja migrate
    when:
      event: [ push, tag, pull_request ]

  - name: test
    image: vikunja/golang-build:latest
    pull: true
    commands:
      - make generate
      - make test
    depends_on: [ fetch-tags ]
    when:
      event: [ push, tag, pull_request ]

  - name: test-sqlite
    image: vikunja/golang-build:latest
    pull: true
    environment:
      VIKUNJA_TESTS_USE_CONFIG: 1
      VIKUNJA_DATABASE_TYPE: sqlite
    commands:
      - make generate
      - make test
    depends_on: [ fetch-tags ]
    when:
      event: [ push, tag, pull_request ]

  - name: test-mysql
    image: vikunja/golang-build:latest
    pull: true
    environment:
      VIKUNJA_TESTS_USE_CONFIG: 1
      VIKUNJA_DATABASE_TYPE: mysql
      VIKUNJA_DATABASE_HOST: test-mysql-unit
      VIKUNJA_DATABASE_USER: root
      VIKUNJA_DATABASE_PASSWORD: vikunjatest
      VIKUNJA_DATABASE_DATABASE: vikunjatest
    commands:
      - make generate
      - make test
    depends_on: [ fetch-tags ]
    when:
      event: [ push, tag, pull_request ]

  - name: test-postgres
    image: vikunja/golang-build:latest
    pull: true
    environment:
      VIKUNJA_TESTS_USE_CONFIG: 1
      VIKUNJA_DATABASE_TYPE: postgres
      VIKUNJA_DATABASE_HOST: test-postgres-unit
      VIKUNJA_DATABASE_USER: postgres
      VIKUNJA_DATABASE_PASSWORD: vikunjatest
      VIKUNJA_DATABASE_DATABASE: vikunjatest
      VIKUNJA_DATABASE_SSLMODE: disable
    commands:
      - make generate
      - make test
    depends_on: [ fetch-tags ]
    when:
      event: [ push, tag, pull_request ]

  - name: integration-test
    image: vikunja/golang-build:latest
    pull: true
    commands:
      - make generate
      - make integration-test
    depends_on: [ fetch-tags ]
    when:
      event: [ push, tag, pull_request ]

  - name: integration-test-sqlite
    image: vikunja/golang-build:latest
    pull: true
    environment:
      VIKUNJA_TESTS_USE_CONFIG: 1
      VIKUNJA_DATABASE_TYPE: sqlite
    commands:
      - make generate
      - make integration-test
    depends_on: [ fetch-tags ]
    when:
      event: [ push, tag, pull_request ]

  - name: integration-test-mysql
    image: vikunja/golang-build:latest
    pull: true
    environment:
      VIKUNJA_TESTS_USE_CONFIG: 1
      VIKUNJA_DATABASE_TYPE: mysql
      VIKUNJA_DATABASE_HOST: test-mysql-integration
      VIKUNJA_DATABASE_USER: root
      VIKUNJA_DATABASE_PASSWORD: vikunjatest
      VIKUNJA_DATABASE_DATABASE: vikunjatest
    commands:
      - make generate
      - make integration-test
    depends_on: [ fetch-tags ]
    when:
      event: [ push, tag, pull_request ]

  - name: integration-test-postgres
    image: vikunja/golang-build:latest
    pull: true
    environment:
      VIKUNJA_TESTS_USE_CONFIG: 1
      VIKUNJA_DATABASE_TYPE: postgres
      VIKUNJA_DATABASE_HOST: test-postgres-integration
      VIKUNJA_DATABASE_USER: postgres
      VIKUNJA_DATABASE_PASSWORD: vikunjatest
      VIKUNJA_DATABASE_DATABASE: vikunjatest
      VIKUNJA_DATABASE_SSLMODE: disable
    commands:
      - make generate
      - make integration-test
    depends_on: [ fetch-tags ]
    when:
      event: [ push, tag, pull_request ]

---
########
# Build a release when pushing to master
# We need to copy this because drone currently can't run a pipeline on either a push to master or a tag.
########

kind: pipeline
name: deploy-master
depends_on:
  - testing

workspace:
  base: /go
  path: src/code.vikunja.io/api

trigger:
  branch:
    - master
  event:
    - push

steps:
  # Needed to get the versions right as they depend on tags
  - name: fetch-tags
    image: docker:git
    commands:
      - git fetch --tags

  - name: before-static-build
    image: techknowlogick/xgo:latest
    pull: true
    commands:
      - export PATH=$PATH:$GOPATH/bin
      - make generate
      - make release-dirs
    depends_on: [ fetch-tags ]

  - name: static-build-windows
    image: techknowlogick/xgo:latest
    pull: true
    environment:
      # This path does not exist. However, when we set the gopath to /go, the build fails. Not sure why.
      # Leaving this here until we know how to resolve this properly.
      GOPATH: /srv/app
    commands:
      - export PATH=$PATH:$GOPATH/bin
      - make release-windows
    depends_on: [ before-static-build ]

  - name: static-build-linux
    image: techknowlogick/xgo:latest
    pull: true
    environment:
      # This path does not exist. However, when we set the gopath to /go, the build fails. Not sure why.
      # Leaving this here until we know how to resolve this properly.
      GOPATH: /srv/app
    commands:
      - export PATH=$PATH:$GOPATH/bin
      - make release-linux
    depends_on: [ before-static-build ]

  - name: static-build-darwin
    image: techknowlogick/xgo:latest
    pull: true
    environment:
      # This path does not exist. However, when we set the gopath to /go, the build fails. Not sure why.
      # Leaving this here until we know how to resolve this properly.
      GOPATH: /srv/app
    commands:
      - export PATH=$PATH:$GOPATH/bin
      - make release-darwin
    depends_on: [ before-static-build ]

  - name: after-build-compress
    image: kolaente/upx
    pull: true
    depends_on:
      - static-build-windows
      - static-build-linux
      - static-build-darwin
    commands:
      - make release-compress

  - name: after-build-static
    image: techknowlogick/xgo:latest
    pull: true
    depends_on:
      - after-build-compress
    commands:
      - make release-copy
      - make release-check
      - make release-os-package
      - make release-zip

  - name: sign-release
    image: plugins/gpgsign:1
    pull: true
    depends_on: [ after-build-static ]
    settings:
      key:
        from_secret: gpg_privkey
      passphrase:
        from_secret: gpg_password
      files:
        - dist/zip/*
      detach_sign: true

  # Push the releases to our pseudo-s3-bucket
  - name: release-latest
    image: plugins/s3:1
    pull: true
    settings:
      bucket: vikunja
      access_key:
        from_secret: aws_access_key_id
      secret_key:
        from_secret: aws_secret_access_key
      endpoint: https://storage.kolaente.de
      path_style: true
      strip_prefix: dist/zip/
      source: dist/zip/*
      target: /api/master/
    depends_on: [ sign-release ]

  # Build a debian package and push it to our bucket
  - name: build-deb
    image: kolaente/fpm
    pull: true
    commands:
      - make build-deb
    depends_on: [ static-build-linux ]

  - name: deb-structure
    image: kolaente/reprepro
    pull: true
    environment:
      GPG_PRIVATE_KEY:
        from_secret: gpg_privatekey
    commands:
      - export GPG_TTY=$(tty)
      - gpg -qk
      - echo "use-agent" >>  ~/.gnupg/gpg.conf
      - gpgconf --kill gpg-agent
      - echo $GPG_PRIVATE_KEY > ~/frederik.gpg
      - gpg --import ~/frederik.gpg
      - mkdir debian/conf -p
      - cp build/reprepro-dist-conf debian/conf/distributions
      - make reprepro
    depends_on: [ build-deb ]

  # Push the releases to our pseudo-s3-bucket
  - name: release-deb
    image: plugins/s3:1
    pull: true
    settings:
      bucket: vikunja
      access_key:
        from_secret: aws_access_key_id
      secret_key:
        from_secret: aws_secret_access_key
      endpoint: https://storage.kolaente.de
      path_style: true
      strip_prefix: debian
      source: debian/*/*/*/*/*
      target: /deb/
    depends_on: [ deb-structure ]

  # Build the docker image and push it to docker hub
  - name: docker
    image: plugins/docker
    pull: true
    settings:
      username:
        from_secret: docker_username
      password:
        from_secret: docker_password
      repo: vikunja/api
      auto_tag: true
    depends_on: [ fetch-tags ]

  - name: telegram
    image: appleboy/drone-telegram
    depends_on:
      - release-latest
    settings:
      token:
        from_secret: TELEGRAM_TOKEN
      to:
        from_secret: TELEGRAM_TO
      message: >
        {{repo.owner}}/{{repo.name}}: \[{{build.status}}] Build {{build.number}}
        {{commit.author}} pushed to {{commit.branch}} {{commit.sha}}: `{{commit.message}}`
        Build started at {{datetime build.started "2006-Jan-02T15:04:05Z" "GMT+2"}} finished at {{datetime build.finished "2006-Jan-02T15:04:05Z" "GMT+2"}}.
    when:
      status:
        - success
        - failure
---
########
# Build a release when tagging
# We need to copy this because drone currently can't run a pipeline on either a push to master or a tag.
########

kind: pipeline
name: deploy-version
depends_on:
  - testing

workspace:
  base: /go
  path: src/code.vikunja.io/api

trigger:
  event:
    - tag

steps:
  - name: fetch-tags
    image: docker:git
    commands:
      - git fetch --tags

  - name: before-static-build
    image: techknowlogick/xgo:latest
    pull: true
    commands:
      - make generate
      - make release-dirs
    depends_on: [ fetch-tags ]

  - name: static-build-windows
    image: techknowlogick/xgo:latest
    pull: true
    environment:
      # This path does not exist. However, when we set the gopath to /go, the build fails. Not sure why.
      # Leaving this here until we know how to resolve this properly.
      GOPATH: /srv/app
    commands:
      - export PATH=$PATH:$GOPATH/bin
      - make release-windows
    depends_on: [ before-static-build ]

  - name: static-build-linux
    image: techknowlogick/xgo:latest
    pull: true
    environment:
      # This path does not exist. However, when we set the gopath to /go, the build fails. Not sure why.
      # Leaving this here until we know how to resolve this properly.
      GOPATH: /srv/app
    commands:
      - export PATH=$PATH:$GOPATH/bin
      - make release-linux
    depends_on: [ before-static-build ]

  - name: static-build-darwin
    image: techknowlogick/xgo:latest
    pull: true
    environment:
      # This path does not exist. However, when we set the gopath to /go, the build fails. Not sure why.
      # Leaving this here until we know how to resolve this properly.
      GOPATH: /srv/app
    commands:
      - export PATH=$PATH:$GOPATH/bin
      - make release-darwin
    depends_on: [ before-static-build ]

  - name: after-build-compress
    image: kolaente/upx
    pull: true
    depends_on:
      - static-build-windows
      - static-build-linux
      - static-build-darwin
    commands:
      - make release-compress

  - name: after-build-static
    image: techknowlogick/xgo:latest
    pull: true
    depends_on:
      - after-build-compress
    commands:
      - make release-copy
      - make release-check
      - make release-os-package
      - make release-zip

  - name: sign-release
    image: plugins/gpgsign:1
    pull: true
    depends_on: [ after-build-static ]
    settings:
      key:
        from_secret: gpg_privkey
      passphrase:
        from_secret: gpg_password
      files:
        - dist/zip/*
      detach_sign: true

  # Push the releases to our pseudo-s3-bucket
  - name: release-version
    image: plugins/s3:1
    pull: true
    settings:
      bucket: vikunja
      access_key:
        from_secret: aws_access_key_id
      secret_key:
        from_secret: aws_secret_access_key
      endpoint: https://storage.kolaente.de
      path_style: true
      strip_prefix: dist/zip/
      source: dist/zip/*
      target: /api/${DRONE_TAG##v}/
    depends_on: [ sign-release ]

  # Build a debian package and push it to our bucket
  - name: build-deb
    image: kolaente/fpm
    pull: true
    commands:
      - make build-deb
    depends_on: [ static-build-linux ]

  - name: deb-structure
    image: kolaente/reprepro
    pull: true
    environment:
      GPG_PRIVATE_KEY:
        from_secret: gpg_privatekey
    commands:
      - export GPG_TTY=$(tty)
      - gpg -qk
      - echo "use-agent" >>  ~/.gnupg/gpg.conf
      - gpgconf --kill gpg-agent
      - echo $GPG_PRIVATE_KEY > ~/frederik.gpg
      - gpg --import ~/frederik.gpg
      - mkdir debian/conf -p
      - cp build/reprepro-dist-conf debian/conf/distributions
      - make reprepro
    depends_on: [ build-deb ]

  # Push the releases to our pseudo-s3-bucket
  - name: release-deb
    image: plugins/s3:1
    pull: true
    settings:
      bucket: vikunja
      access_key:
        from_secret: aws_access_key_id
      secret_key:
        from_secret: aws_secret_access_key
      endpoint: https://storage.kolaente.de
      path_style: true
      strip_prefix: debian
      source: debian/*/*/*/*/*
      target: /deb/
    depends_on: [ deb-structure ]

  # Build the docker image and push it to docker hub
  - name: docker
    image: plugins/docker
    pull: true
    settings:
      username:
        from_secret: docker_username
      password:
        from_secret: docker_password
      repo: vikunja/api
      auto_tag: true
    depends_on: [ fetch-tags ]

  - name: telegram
    image: appleboy/drone-telegram
    depends_on:
      - docker
      - release-version
    settings:
      token:
        from_secret: TELEGRAM_TOKEN
      to:
        from_secret: TELEGRAM_TO
      message: >
        {{repo.owner}}/{{repo.name}}: \[{{build.status}}] Build {{build.number}}
        {{commit.author}} pushed to {{commit.branch}} {{commit.sha}}: `{{commit.message}}`
        Build started at {{datetime build.started "2006-Jan-02T15:04:05Z" "GMT+2"}} finished at {{datetime build.finished "2006-Jan-02T15:04:05Z" "GMT+2"}}.
    when:
      status:
        - success
        - failure

---
kind: pipeline
name: deploy-docs

workspace:
  base: /go
  path: src/code.vikunja.io/api

clone:
  depth: 50

trigger:
  event:
    - push
  branch:
    - master

steps:
  - name: submodules
    image: docker:git
    commands:
      - git submodule update --init
      - git submodule update --recursive --remote

  - name: theme
    image: kolaente/yarn
    pull: true
    group: build-static
    commands:
      - cd docs/themes/vikunja
      - yarn --production=false
      - ./node_modules/.bin/gulp prod

  - name: build
    image: monachus/hugo:v0.54.0
    pull: true
    commands:
      - cd docs
      - hugo
      - mv public/docs/* public # Hugo seems to be not capable of setting a different theme for a home page, so we do this ugly hack to fix it.

  - name: docker
    image: plugins/docker
    pull: true
    settings:
      username:
        from_secret: docker_username
      password:
        from_secret: docker_password
      repo: vikunja/docs
      context: docs/
      dockerfile: docs/Dockerfile
