module gitea.com/xorm/xorm-redis-cache

go 1.13

require (
	gitea.com/xorm/tests v0.7.0
	github.com/garyburd/redigo v1.6.0
	github.com/go-sql-driver/mysql v1.4.1
	xorm.io/xorm v1.0.1
)
