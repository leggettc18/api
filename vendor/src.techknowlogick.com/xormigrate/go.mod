module src.techknowlogick.com/xormigrate

go 1.14

require (
	github.com/denisenkom/go-mssqldb v0.0.0-20200428022330-06a60b6afbbc
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.6.0
	github.com/mattn/go-sqlite3 v1.14.0
	github.com/stretchr/testify v1.6.0
	golang.org/x/text v0.3.2 // indirect
	xorm.io/xorm v1.0.1
)
